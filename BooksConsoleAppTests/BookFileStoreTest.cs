﻿using BooksConsoleApp.Models;
using BooksConsoleApp.Services.Implementation;

namespace BookDataProcessorTests
{
    public class BookFileStoreTest
    {
        private readonly string fileName = "result.txt";

        private List<Book> GetTestBooks()
        {
            return new List<Book>
            {
                new Book
                {
                    Id = 1,
                    ParentName = "Parent1",
                    DisplayName = "Book1",
                    Meta = new Meta { States = new List<string> { "State1", "State2" } }
                }
            };
        }

        [Fact]
        public async Task FileStoreAsync_WritesPassedDataSuccessfullyToFileInGivenFormat()
        {
            // Arrange
            var booksFileStore = new BooksFileStore();
            var books = GetTestBooks();

            var groupedBooks = books.GroupBy(b => b.ParentName).ToList();

            var expectedStringInFile = $"Display name: Book1 - state: State2";

            // Act
            await booksFileStore.FileStoreAsync(groupedBooks);

            // Assert
            var fileContent = File.ReadAllText(fileName);
            Assert.Contains(expectedStringInFile, fileContent);
        }

        [Fact]
        public async Task FileStoreAsync_FailIfFileContainsUnExpectedData()
        {
            // Arrange
            var booksFileStore = new BooksFileStore();

            var books = GetTestBooks();

            var groupedBooks = books.GroupBy(b => b.ParentName).ToList();

            var expectedData = "\nParent name: Parent1\r\nDisplay name: Book1 - state: State1\r\nDisplay name: Book1 - state: State2\r\n";

            // Act
            await booksFileStore.FileStoreAsync(groupedBooks);

            // Assert
            var fileContent = File.ReadAllText(fileName);

            Assert.Equal(expectedData, fileContent);
        }
    }
}