using BooksConsoleApp.ApiClients.Implementation;
using BooksConsoleApp.ApiClients.Interface;
using Moq;

namespace BookDataProcessorTests
{
    public class BooksClientApiTests
    {
        [Fact]
        public async Task GetBooksAsync_Success()
        {
            // Arrange
            var httpClientFactoryMock = new Mock<IHttpClientFactory>();

            var httpClientMock = new Mock<HttpClient>();

            httpClientFactoryMock.Setup(factory => factory.CreateClient(It.IsAny<string>())).Returns(httpClientMock.Object);

            var bookApiClient = new BookApiClient(httpClientFactoryMock.Object);

            // Act
            var result = await bookApiClient.GetBooksAsync();

            // Assert
            Assert.NotNull(result);
            Assert.True(result.BooksList.Count > 0);
        }
    }
}