﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using BooksConsoleApp.ApiClients.Interface;
using BooksConsoleApp.ApiClients.Implementation;
using BooksConsoleApp.Services.Interface;
using BooksConsoleApp.Services.Implementation;
using BooksConsoleApp.Models;
using System.Reflection;
using System.Net;

namespace BooksConsoleApp
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            var builder = new HostBuilder()
            .ConfigureServices((hostContext, services) =>
            {
                services.AddHttpClient("myHttpClient", client =>
                {
                    client.DefaultRequestHeaders.AcceptEncoding.Add(new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));
                }).ConfigurePrimaryHttpMessageHandler(() =>
                {
                    return new HttpClientHandler
                    {
                        AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                    };
                });

                services.AddSingleton<IBookApiClient, BookApiClient>();
                services.AddScoped<IBookService, BookService>();
                services.AddScoped<IBooksStoreData, BooksFileStore>();

            }).UseConsoleLifetime();

            var host = builder.Build();

            try
            {
                Console.WriteLine("Application has been started");

                Func<Book, bool> booksFilter = book =>
                (
                book.Meta.States.Any(state => state == "CO" || state == "NJ")
                && book.ParentName != null
                );

                PropertyInfo byProperty = typeof(Book).GetProperty(nameof(Book.ParentName));
                Func<List<Book>, object> groupBooks = books => books.GroupBy(book => byProperty.GetValue(book))
                                                                    .OrderBy(group => group.Key);

                var bookService = host.Services.GetRequiredService<IBookService>();
                var result = await bookService.BooksProcessData(booksFilter, groupBooks);

                Console.WriteLine("Application execution has completed.");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred: {ex.Message}");
            } 
        }
    }
}