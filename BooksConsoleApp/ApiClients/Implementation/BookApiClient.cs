﻿using Newtonsoft.Json;
using BooksConsoleApp.ApiClients.Interface;
using BooksConsoleApp.Models;

namespace BooksConsoleApp.ApiClients.Implementation
{
    public class BookApiClient : IBookApiClient
    {
        private readonly string booksApiURL = "https://api.actionnetwork.com/web/v1/books";
        private readonly IHttpClientFactory _httpClientFactory;
        
        public BookApiClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<Books> GetBooksAsync()
        {
            using var httpClient = _httpClientFactory.CreateClient("myHttpClient");
            {
                Books result = null;
                try
                {
                    HttpResponseMessage response = await httpClient.GetAsync(booksApiURL);

                    if (response.IsSuccessStatusCode)
                    {
                        string json = await response.Content.ReadAsStringAsync();
                        result = JsonConvert.DeserializeObject<Books>(json);
                    }
                    else
                    {
                        throw new Exception($"Request failed with status code {response.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"An error occurred: {ex.Message}");
                }
                return result;
            }
        }
    }
}