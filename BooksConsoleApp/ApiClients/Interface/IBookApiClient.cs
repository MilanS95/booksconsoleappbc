﻿using BooksConsoleApp.Models;

namespace BooksConsoleApp.ApiClients.Interface
{
    public interface IBookApiClient
    {
        Task<Books> GetBooksAsync();
    }
}