﻿using Newtonsoft.Json;

namespace BooksConsoleApp.Models
{
    public class Meta
    {
        [JsonProperty("states")]
        public List<string> States { get; set; }
    }
}