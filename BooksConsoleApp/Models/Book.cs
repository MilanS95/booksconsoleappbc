﻿using Newtonsoft.Json;

namespace BooksConsoleApp.Models
{
    public class Book
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }

        [JsonProperty("abbr")]
        public string Abbr { get; set; }

        [JsonProperty("source_name")]
        public string SourceName { get; set; }

        [JsonProperty("meta")]
        public Meta Meta { get; set; }

        [JsonProperty("parent_name")]
        public string ParentName { get; set; }
        
        [JsonProperty("book_parent_id")]
        public int? BookParentId { get; set; }

        [JsonProperty("affiliate_id")]
        public int? AffiliateId { get; set; }
    }
}