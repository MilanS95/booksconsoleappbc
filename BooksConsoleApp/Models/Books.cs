﻿using Newtonsoft.Json;

namespace BooksConsoleApp.Models
{
    public class Books
    {
        [JsonProperty("books")]
        public List<Book> BooksList { get; set; }
    }
}