﻿using BooksConsoleApp.Models;

namespace BooksConsoleApp.Services.Interface
{
    public interface IBookService
    {
        Task<List<Book>> BooksProcessData(Func<Book,bool> filterBy = null, Func<List<Book>,object> groupAndSortBy = null);
    }
}