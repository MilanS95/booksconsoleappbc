﻿using BooksConsoleApp.Models;

namespace BooksConsoleApp.Services.Interface
{
    public interface IBooksStoreData
    {
        Task FileStoreAsync(IEnumerable<IGrouping<object, Book>> groupedBooks);
    }
}