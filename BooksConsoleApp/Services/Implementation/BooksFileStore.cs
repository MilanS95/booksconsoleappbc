﻿using BooksConsoleApp.Models;
using BooksConsoleApp.Services.Interface;

namespace BooksConsoleApp.Services.Implementation
{
    public class BooksFileStore : IBooksStoreData
    {
        private readonly string _fileName = "result.txt";

        public async Task FileStoreAsync(IEnumerable<IGrouping<object, Book>> groupedBooks)
        {
            if (groupedBooks == null)
            {
                Console.WriteLine("There are no grouped books available for writing into the file. The provided list is empty.");
                return;
            }
            try
            {
                using (var writer = new StreamWriter(_fileName))
                {
                    foreach (var group in groupedBooks)
                    {
                        await writer.WriteLineAsync($"\nParent name: {group.Key}"); //{book.parent_name}

                        foreach (var book in group)
                            foreach (var state in book.Meta.States)
                                await writer.WriteLineAsync($"Display name: {book.DisplayName} - state: {state}");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while storing data", ex);
            }
        }
    }
}