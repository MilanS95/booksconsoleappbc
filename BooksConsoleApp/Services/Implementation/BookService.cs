﻿using BooksConsoleApp.ApiClients.Interface;
using BooksConsoleApp.Models;
using BooksConsoleApp.Services.Interface;

namespace BooksConsoleApp.Services.Implementation
{
    public class BookService : IBookService
    {
        private readonly IBookApiClient _bookApiClient;
        private readonly IBooksStoreData _booksStoreData;
        public BookService(IBookApiClient bookApiClient, IBooksStoreData booksStoreData)
        {
            _bookApiClient = bookApiClient;
            _booksStoreData = booksStoreData;
        }

        public async Task<List<Book>> BooksProcessData(Func<Book, bool> filterBy, Func<List<Book>, object> groupAndSortBy)
        {
            List<Book> books = (await _bookApiClient.GetBooksAsync())?.BooksList;

            if (books == null)
            {
                return books;
            }

            if (filterBy != null)
            {
                books = FilterBooks(books, filterBy);
            }

            if (groupAndSortBy != null)
            {
                await _booksStoreData.FileStoreAsync(GroupBooks(books, groupAndSortBy));
            }
            return books;
        }

        private List<Book> FilterBooks(List<Book> books, Func<Book, bool> filterBy)
        {
            return books.Where(filterBy).ToList();
        }

        private IEnumerable<IGrouping<object, Book>> GroupBooks(List<Book> books, Func<List<Book>, object> groupAndSortBy)
        {
            return groupAndSortBy(books) as IEnumerable<IGrouping<object, Book>>;
        }
    }
}